class ModelPoints{
  final String id;
  final double? geoLat;
  final double? geoLong;

  ModelPoints({required this.id, required this.geoLat, required this.geoLong});

  static ModelPoints fromJson(Map<String, dynamic> json){
    return ModelPoints(
        id: json['id'],
        geoLat: json['latitude'],
        geoLong: json['longitude']
    );
  }
}