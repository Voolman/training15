class ModelMessage {
  final String id;
  final String idChat;
  final String message;
  final String createdAt;
  final String idUser;

  ModelMessage({required this.id,
    required this.idChat,
    required this.message,
    required this.createdAt,
    required this.idUser});

  static ModelMessage fromJson(Map<String, dynamic> json) {
    return ModelMessage(
        id: json['id'],
        idChat: json['id_chat'],
        message: json['message'],
        createdAt: json['created_at'],
        idUser: json['id_user']
    );
  }
}
