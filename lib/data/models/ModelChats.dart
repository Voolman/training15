class ModelChats {
  final String id;
  final String firstUserId;
  final String secondUserId;
  final String createdAt;

  ModelChats({required this.id,
    required this.firstUserId,
    required this.secondUserId,
    required this.createdAt});

  static ModelChats fromJson(Map<String, dynamic> json) {
    return ModelChats(
        id: json['id'],
        firstUserId: json['first_user_id'],
        secondUserId: json['second_user_id'],
        createdAt: json['created_at']
    );
  }
}
