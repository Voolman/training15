class ModelPackage {
  final String id;
  final String pointAddress;
  final String pointCountry;
  final String pointPhone;
  final String? pointOthers;
  final String packageItem;
  final int packageWeight;
  final int packageWorth;

  ModelPackage({
    required this.id,
    required this.pointAddress,
    required this.pointCountry,
    required this.pointPhone,
    required this.pointOthers,
    required this.packageItem,
    required this.packageWeight,
    required this.packageWorth});

  static ModelPackage fromJson(Map<String, dynamic> json) {
    return ModelPackage(
        id: json['id_user'],
        pointAddress: json['address'],
        pointCountry: json['country'],
        pointPhone: json['phone'],
        pointOthers: json['others'],
        packageItem: json['package_items'],
        packageWeight: json['weight_items'],
        packageWorth: json['worth_items']
    );
  }
}
