import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/data/repository/supabase.dart';

Future<void> getUserChats(Function onResponse, Function(String) onError) async {
  try{
    var response = await getChats();
    var secondUsers = [];
    for (int i =0; i< response.length; i++){
      var user = getUserFromId(response[i].secondUserId);
      secondUsers.add(user);
    }
    onResponse(secondUsers.reversed);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}