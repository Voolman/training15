import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_3/data/models/ModelMessage.dart';
import 'package:training_final_3/data/repository/supabase.dart';

Future<void> getMessages(String id, Function onResponse, Function(String) onError) async {
  try{
    List<ModelMessage> res = await getUsersMessages(id);
    onResponse(res.reversed);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}