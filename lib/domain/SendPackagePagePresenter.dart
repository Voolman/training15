import 'package:supabase_flutter/supabase_flutter.dart';
import '../data/repository/supabase.dart';

Future<void> getPackageInformation(Function onResponse, Function(String) onError) async {
  try{
    var response = await getOrderInformation();
    var currentId = getCurrentUserId();
    var res = response.where((element) => element.id == currentId).single;
    var response1 = await getDestinationInformation();
    var res1 = response1.where((element) => element.id == currentId).single;
    onResponse(res, res1);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e) {
    onError(e.toString());
  }
}

