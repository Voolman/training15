import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:training_final_3/domain/HomePagePresenter.dart';
import 'package:training_final_3/presentation/theme/colors.dart';

import '../../../data/models/ModelProfile.dart';
import '../../../domain/HomePresenter.dart';
import '../../widgets/dialogs.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}
var name = '';
List news = [];
bool box1 = false;
bool box2 = false;
bool box3 = false;
bool box4 = false;
Uint8List avatar = '' as Uint8List;
int currentIndex = 2;
class _HomePageState extends State<HomePage> {
  @override
  void initState(){
    super.initState();
    getProfile(
            (ModelProfile currentUser){
          setState(() {
            name = currentUser.fullName;
          });
        },
            (String e) {showError(context, e);}
    );
    getNews(
        (List res){
          setState(() {
            news = res;
          });
        },
        (String e) {showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 69, left: 24, right: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 34,
              width: double.infinity,
              decoration: BoxDecoration(
                color: colors.hint,
                borderRadius: BorderRadius.circular(4)
              ),
              child: TextField(
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  hintText: 'Search services',
                  hintStyle: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: colors.subtext
                  ),
                  suffixIcon: SvgPicture.asset(
                      'assets/search2.svg',
                      fit: BoxFit.scaleDown,
                      colorFilter: ColorFilter.mode(colors.subtext, BlendMode.dst))
                ),
              ),
            ),
            const SizedBox(height: 24),
            Container(
              height: 91,
              width: double.infinity,
              decoration: BoxDecoration(
                color: colors.primary,
                borderRadius: BorderRadius.circular(8)
              ),
              child: Row(
                children: [
                  const SizedBox(width: 10,),
                  Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: colors.hint
                      ),
                      child: Icon(Icons.person, color: colors.text)
                  ),
                  Padding(
                      padding: const EdgeInsets.symmetric(vertical: 23, horizontal: 10),
                    child: Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Hello $name',
                            style: Theme.of(context).textTheme.titleLarge?.copyWith(color: colors.background),
                          ),
                          Text(
                            'We trust you are having a great time',
                            style: TextStyle(
                              color: colors.hint,
                              fontWeight: FontWeight.w400,
                              fontSize: 10,
                              height: 4/3
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  const Expanded(child: SizedBox()),
                  SvgPicture.asset('assets/ring.svg'),
                  const SizedBox(width: 10)
                ],
              ),
            ),
            const SizedBox(height: 39,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  'Special for you',
                  style: TextStyle(
                    color: colors.secondary,
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    height: 8/7
                  ),
                ),
                Image.asset('assets/arrow.png')
              ],
            ),
            const SizedBox(height: 7),
            SizedBox(
              height: 64,
              width: double.infinity,
              child: PageView.builder(
                itemCount: news.length ~/ 2,
                  onPageChanged: (newIndex){
                    setState(() {
                      (newIndex == 1) ? currentIndex = newIndex + 1 : currentIndex = newIndex;
                    });
                  },
                  itemBuilder: (_, index){
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                          Container(
                            height: 64,
                            width: 166,
                            decoration: BoxDecoration(
                              border: Border.all(color: colors.secondary, width: 1),
                              borderRadius: BorderRadius.circular(8)
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                                child: Image.network(news[currentIndex],fit: BoxFit.cover)),
                          ),

                          Container(
                            height: 64,
                            width: 166,
                            decoration: BoxDecoration(
                                border: Border.all(color: colors.secondary, width: 1),
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.network(news[currentIndex+1], fit: BoxFit.cover,),
                        )
                      )
                    ],
                  );
                }
              ),
            ),
            const SizedBox(height: 29),
            Text(
              'What would you like to do',
              style: TextStyle(
                color: colors.primary,
                fontWeight: FontWeight.w500,
                fontSize: 14,
                height: 8/7
              ),
            ),

            const SizedBox(height: 10),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTapDown: (i){
                        setState(() {
                          box1 = !box1;
                        });
                      },
                      onTapUp: (i){
                        setState(() {
                          box1 = !box1;
                        });
                      },
                      child: Container(
                        height: 159,
                        width: 159,
                        decoration: BoxDecoration(
                          color: (box1) ? colors.primary : const Color.fromARGB(255, 242, 242, 242),
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromARGB(38, 0, 0, 0),
                              blurRadius: 2,
                              offset: Offset(0, 2),
                            )
                          ]
                        ),
                        child: Padding(
                            padding: const EdgeInsets.only(left: 8, right: 24),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SvgPicture.asset('assets/head.svg',
                              color: (box1) ? Colors.white : colors.primary),
                              const SizedBox(height: 6),
                              Text(
                                'Customer Care',
                                style: TextStyle(
                                  color: (box1) ? Colors.white : colors.primary,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  height: 1
                                ),
                              ),
                              const SizedBox(height: 6,),
                              Flexible(
                                  child: Text(
                                    'Our customer care service line is available from 8 -9pm week days and 9 - 5 weekends - tap to call us today',
                                    style: TextStyle(
                                      color: (box1) ? Colors.white : colors.text,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 7.45
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                      ),
                    ),

                    GestureDetector(
                      onTapDown: (i){
                        setState(() {
                          box2 = !box2;
                        });
                      },
                      onTapUp: (i){
                        setState(() {
                          box2 = !box2;
                        });
                      },
                      child: Container(
                        height: 159,
                        width: 159,
                        decoration: BoxDecoration(
                            color: (box2) ? colors.primary : const Color.fromARGB(255, 242, 242, 242),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromARGB(38, 0, 0, 0),
                                blurRadius: 2,
                                offset: Offset(0, 2),
                              )
                            ]
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8, right: 24),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SvgPicture.asset(
                                  'assets/box.svg',
                                  color: (box2) ? Colors.white : colors.primary ),
                              const SizedBox(height: 6),
                              Text(
                                'Send a package',
                                style: TextStyle(
                                    color: (box2) ? Colors.white : colors.primary,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1
                                ),
                              ),
                              const SizedBox(height: 6,),
                              Flexible(
                                  child: Text(
                                    'Request for a driver to pick up or deliver your package for you',
                                    style: TextStyle(
                                      color: (box2) ? Colors.white : colors.text,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 7.45
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 23),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTapDown: (i){
                        setState(() {
                          box3 = !box3;
                        });
                      },
                      onTapUp: (i){
                        setState(() {
                          box3 = !box3;
                        });
                      },
                      child: Container(
                        height: 159,
                        width: 159,
                        decoration: BoxDecoration(
                            color: (box3) ? colors.primary : const Color.fromARGB(255, 242, 242, 242),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromARGB(38, 0, 0, 0),
                                blurRadius: 2,
                                offset: Offset(0, 2),
                              )
                            ]
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8, right: 24),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SvgPicture.asset('assets/call-centre.svg',
                              color: (box3) ? Colors.white : colors.primary),
                              const SizedBox(height: 6),
                              Text(
                                'Fund your wallet',
                                style: TextStyle(
                                    color: (box3) ? Colors.white : colors.primary,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1
                                ),
                              ),
                              const SizedBox(height: 6,),
                              Flexible(
                                child: Text(
                                  'To fund your wallet is as easy as ABC, make use of our fast technology and top-up your wallet today',
                                  style: TextStyle(
                                    color: (box3) ? Colors.white : colors.text,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 7.45
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),

                    GestureDetector(
                      onTapDown: (i){
                        setState(() {
                          box4 = !box4;
                        });
                      },
                      onTapUp: (i){
                        setState(() {
                          box4 = !box4;
                        });
                      },
                      child: Container(
                        height: 159,
                        width: 159,
                        decoration: BoxDecoration(
                            color: (box4) ? colors.primary : const Color.fromARGB(255, 242, 242, 242),
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromARGB(38, 0, 0, 0),
                                blurRadius: 2,
                                offset: Offset(0, 2),
                              )
                            ]
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8, right: 24),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SvgPicture.asset('assets/chats.svg',
                              color: (box4) ? Colors.white : colors.primary),
                              const SizedBox(height: 6),
                              Text(
                                'Chats',
                                style: TextStyle(
                                    color: (box4) ? Colors.white : colors.primary,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    height: 1
                                ),
                              ),
                              const SizedBox(height: 6,),
                              Flexible(
                                  child: Text(
                                    'Search for available rider within your area',
                                    style: TextStyle(
                                      color: (box4) ? Colors.white : colors.text,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 7.45
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}