import 'package:flutter/material.dart';
import 'package:training_final_3/domain/HomePresenter.dart';
import 'package:training_final_3/presentation/pages/SendPackagePage.dart';
import 'package:training_final_3/presentation/widgets/dialogs.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import '../../../data/models/ModelProfile.dart';
import '../../theme/colors.dart';

class Track extends StatefulWidget {
  const Track({super.key});

  @override
  State<Track> createState() => _TrackState();
}

List<PlacemarkMapObject> mapPoints = [];
String id = '';

class _TrackState extends State<Track> {
  @override
  void initState() {
    super.initState();
    getProfile(
            (ModelProfile user) {
          setState(() {
            id = user.idUser;
          });
        },
        (String e){showError(context, e);}
    );
  }

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 320,
            width: double.infinity,
            child: YandexMap(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 42, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Tracking Number',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleLarge
                          ?.copyWith(fontSize: 16),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 24,
                ),
                Row(
                  children: [
                    Image.asset('assets/snow.png'),
                    const SizedBox(width: 8),
                    Text(
                      'R-$id',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: colors.primary),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Row(
                  children: [
                    Text(
                      'Package Status',
                      style: TextStyle(
                          color: colors.subtext,
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                const SizedBox(height: 24),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: 21,
                          width: 21,
                          child: Checkbox(
                            value: true,
                            activeColor: colors.primary,
                            side: BorderSide(color: colors.primary, width: 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)),
                            onChanged: (value) {},
                          ),
                        ),
                        Container(
                          height: 34,
                          width: 1,
                          decoration: BoxDecoration(color: colors.subtext),
                        ),
                        SizedBox(
                          height: 21,
                          width: 21,
                          child: Checkbox(
                            value: true,
                            activeColor: colors.primary,
                            side: BorderSide(color: colors.primary, width: 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)),
                            onChanged: (value) {},
                          ),
                        ),
                        Container(
                          height: 34,
                          width: 1,
                          decoration: BoxDecoration(color: colors.subtext),
                        ),
                        SizedBox(
                          height: 21,
                          width: 21,
                          child: Checkbox(
                            value: false,
                            activeColor: colors.primary,
                            side: BorderSide(color: colors.primary, width: 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)),
                            onChanged: (value) {},
                          ),
                        ),
                        Container(
                          height: 34,
                          width: 1,
                          decoration: BoxDecoration(color: colors.subtext),
                        ),
                        Container(
                          height: 21,
                          width: 21,
                          decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 224, 224, 224),
                          ),
                          child: Center(
                            child: Text(
                              '----',
                              style: TextStyle(
                                  color: colors.background, fontSize: 10),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(width: 7),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Courier requested',
                          style: TextStyle(
                              color: colors.subtext,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(height: 4),
                        Text(
                          'April 22 2024  13:24',
                          style: TextStyle(
                              color: colors.secondary,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: 17,
                        ),
                        Text(
                          'Package ready for delivery',
                          style: TextStyle(
                              color: colors.subtext,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(height: 4),
                        Text(
                          'April 22 2024  13:54',
                          style: TextStyle(
                              color: colors.secondary,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: 17,
                        ),
                        Text(
                          'Package in transit',
                          style: TextStyle(
                              color: colors.primary,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(height: 4),
                        Text(
                          'April 22 2024  15:54',
                          style: TextStyle(
                              color: colors.secondary,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(
                          height: 17,
                        ),
                        Text(
                          'Package in transit',
                          style: TextStyle(
                              color: colors.subtext,
                              fontSize: 14,
                              fontWeight: FontWeight.w400),
                        ),
                        SizedBox(height: 4),
                        Text(
                          'April 22 2024  15:54',
                          style: TextStyle(
                              color: colors.subtext,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SendPackagePage()));
                      },
                      style: Theme
                          .of(context)
                          .filledButtonTheme
                          .style,
                      child: Text('View Package Info',
                          style: Theme
                              .of(context)
                              .textTheme
                              .labelMedium)),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
