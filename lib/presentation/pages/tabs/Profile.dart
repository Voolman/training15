import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_3/data/models/ModelProfile.dart';
import 'package:training_final_3/main.dart';
import 'package:training_final_3/presentation/pages/LogIn.dart';
import '../../../domain/HomePresenter.dart';
import '../../widgets/CustomListTile.dart';
import '../../widgets/dialogs.dart';
import '../AddPayment.dart';
import '../Notification.dart';
import '../SendPackage.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}
bool isSee = true;
bool isDark = false;
var name = '';
var balance = '';
class _ProfileState extends State<Profile> {

  @override
  void initState(){
    super.initState();
    getProfile(
        (ModelProfile currentUser){
          setState(() {
            name = currentUser.fullName;
            balance = currentUser.balance.toString();
          });
        },
        (String e) {showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          SizedBox(
            height: 108,
            width: double.infinity,
            child: Column(
              children: [
                const SizedBox(height: 73),
                Center(
                  child: Text(
                    'Profile',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 2,
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(38, 0, 0, 0),
                    blurRadius: 2,
                    offset: Offset(0, 2),
                  )]
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 17, left: 24, right: 24),
            child: Column(
              children: [
                SizedBox(
                  height: 75,
                  child: ListTile(
                    contentPadding: const EdgeInsets.symmetric(vertical: 10),
                    leading: Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: colors.hint
                        ),
                        child: Icon(Icons.person, color: colors.text)
                    ),
                    title: Text(
                      name,
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 16)
                    ),
                    subtitle: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: "Current balance: ",
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 12)
                            ),
                            TextSpan(
                                text: (isSee) ? "N$balance:00" : "*"*5,
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 12, color: colors.primary)
                            )
                          ]
                      ),
                    ),
                    trailing: GestureDetector(
                        onTap: (){
                          setState(() {
                            isSee = !isSee;
                          });
                        },
                        child: SvgPicture.asset('assets/eye-slash.svg')
                    ),
                  ),
                ),
                const SizedBox(height: 19),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Enable dark Mode',
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 16)
                    ),
                    Switch(
                        inactiveThumbColor: Colors.white,
                        inactiveTrackColor: const Color.fromARGB(255, 215, 215, 215),
                        trackOutlineColor: MaterialStateProperty.resolveWith((states) => colors.background),
                        value: isDark,
                        onChanged: (value){
                          setState(() {
                            isDark = value;
                          });
                        }
                    )
                  ],
                ),
                CustomListTile(
                    color: colors.text,
                    icon: Image.asset('assets/profile_page.png'),
                    label: "Edit Profile",
                    text: 'Name, phone no, address, email ...'
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SendPackage()));
                  },
                  child: CustomListTile(
                      icon: Image.asset('assets/paper.png'),
                      color: colors.text,
                      label: "Statements & Reports",
                      text: 'Download transaction details, orders, deliveries'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const NotificationPage()));
                  },
                  child: CustomListTile(
                      icon: Image.asset('assets/notification.png'),
                      color: colors.text,
                      label: "Notification Settings",
                      text: 'mute, unmute, set location & tracking setting'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const AddPaymentPage()));
                  },
                  child: CustomListTile(
                      icon: Image.asset('assets/wallet.png'),
                      color: colors.text,
                      label: "Card & Bank account settings",
                      text: 'change cards, delete card details'
                  ),
                ),
                CustomListTile(
                    icon: Image.asset('assets/two-person.png'),
                    color: colors.text,
                    label: "Referrals",
                    text: 'check no of friends and earn'
                ),
                CustomListTile(
                    icon: Image.asset('assets/map.png'),
                    color: colors.text,
                    label: "About Us",
                    text: 'know more about us, terms and conditions'
                ),
                GestureDetector(
                  onTap: (){
                    showLoading(context);
                    pressSignOut(
                      (){
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const LogIn()), (route) => false);
                        },
                      (String e){showError(context, e);}
                    );
                    Navigator.of(context).pop();
                  },
                  child: CustomListTile(
                    icon: Image.asset('assets/log-out.png'),
                    color: colors.error,
                    label: "Log Out",
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}