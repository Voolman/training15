import 'package:flutter/material.dart';
import 'package:training_final_3/presentation/pages/ForgotPassword.dart';
import 'package:training_final_3/presentation/pages/Home.dart';
import 'package:training_final_3/presentation/pages/SignUp.dart';
import 'package:training_final_3/presentation/theme/colors.dart';
import 'package:training_final_3/presentation/widgets/CustomTextField.dart';

import '../../domain/LogInPresenter.dart';
import '../widgets/dialogs.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});


  @override
  State<LogIn> createState() => _LogInState();
}
bool isChecked = false;
class _LogInState extends State<LogIn> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();
    void onChanged(_){}
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Welcome Back',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Fill in your email and password to continue',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              CustomTextField(
                  label: 'Email Address',
                  hint: '***********@mail.com',
                  onChanged: onChanged,
                  controller: email
              ),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: password
              ),
              const SizedBox(height: 17),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 21,
                        width: 21,
                        child: Checkbox(
                            value: isChecked,
                            activeColor: colors.primary,
                            side: BorderSide(color: colors.subtext, width: 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)
                            ),
                            onChanged: (value){
                              setState(() {
                                isChecked = value!;
                              });
                            }
                        ),
                      ),
                      const SizedBox(width: 4),
                      Text(
                        'Remember password',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const ForgotPassword()));
                    },
                    child: Text(
                      'Forgot Password?',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.primary, fontSize: 12),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 187),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        pressSignIn(
                            email.text,
                            password.text,
                              (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const Home()), (route) => false);},
                              (String e){showError(context, e);}
                        );
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Log in',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
              const SizedBox(height: 20),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const SignUp()));
                },
                child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Already have an account?',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Sign up',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.primary)
                          )
                        ]
                    )
                ),
              ),
              const SizedBox(height: 18),
              Text(
                'or sign in using',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
              ),
              const SizedBox(height: 8),
              Image.asset('assets/google.png')
            ],
          ),
        ),
      ),
    );
  }
}