import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:training_final_3/data/models/ModelProfile.dart';
import 'package:training_final_3/domain/ChatsPresenter.dart';
import 'package:training_final_3/presentation/widgets/dialogs.dart';

import '../theme/colors.dart';

class Chats extends StatefulWidget {
  const Chats({super.key});


  @override
  State<Chats> createState() => _ChatsState();
}
List<ModelProfile> users = [];
class _ChatsState extends State<Chats> {

  void iniState(){
    super.initState();
    getUserChats(
            (res){
          setState(() {
            users = res;
          });
        },
            (String e){showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 108,
            width: double.infinity,
            decoration: BoxDecoration(
                color: colors.background,
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromARGB(38, 0, 0, 0),
                    blurRadius: 5,
                    offset: Offset(0, 2),
                  )
                ]
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 106,
                  width: double.infinity,
                  child: Column(
                    children: [
                      const SizedBox(height: 73,),
                      Row(
                        children: [
                          const SizedBox(width: 15),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Image.asset('assets/back.png'),
                          ),
                          const SizedBox(width: 99),
                          Text(
                            'Chats',
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(fontSize: 16),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 21, left: 23, right: 25),
            child: Column(
              children: [
                Container(
                  height: 40,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: colors.hint,
                      borderRadius: BorderRadius.circular(4)
                  ),
                  child: TextField(
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.transparent),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        hintText: 'Search for a driver',
                        hintStyle: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: colors.subtext
                        ),
                        suffixIcon: SvgPicture.asset(
                            'assets/search2.svg',
                            fit: BoxFit.scaleDown,
                            colorFilter: ColorFilter.mode(colors.subtext, BlendMode.dst))
                    ),
                  ),
                ),
                SizedBox(height: 27),
                SizedBox(
                  height: 585,
                  width: double.infinity,
                  child: ListView.builder(
                    itemCount: users.length,
                      itemBuilder: (_, index){
                      return GestureDetector(
                        onTap: (){

                        },
                        child: Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: colors.background,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: colors.subtext, width: 1)
                          ),
                          child: Center(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(width: 8),
                                Container(
                                    width: 60,
                                    height: 60,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: colors.subtext, width: 1),
                                        shape: BoxShape.circle,
                                        color: colors.hint
                                    ),
                                    child: Icon(Icons.person, color: colors.text)
                                ),
                                SizedBox(width: 12),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 23, horizontal: 10),
                                  child: Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          users[index].fullName,
                                          style: Theme.of(context).textTheme.titleLarge?.copyWith(color: colors.background),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                      }
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}