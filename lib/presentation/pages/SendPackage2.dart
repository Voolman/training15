import 'package:flutter/material.dart';
import 'package:training_final_3/domain/SendPackagePresenter.dart';
import 'package:training_final_3/main.dart';
import 'package:training_final_3/presentation/widgets/dialogs.dart';
import 'Transaction.dart';


class SendPackage2 extends StatefulWidget {
  final String pointAddress;
  final String pointCountry;
  final String pointPhone;
  final String pointOthers;
  final String destinationAddress;
  final String destinationCountry;
  final String destinationPhone;
  final String destinationOthers;
  final String packageItem;
  final String packageWeight;
  final String packageWorth;

  const SendPackage2(
      {super.key, required this.pointAddress, required this.pointCountry, required this.pointPhone, required this.pointOthers, required this.destinationAddress, required this.destinationCountry, required this.destinationPhone, required this.destinationOthers, required this.packageItem, required this.packageWeight, required this.packageWorth});

  @override
  State<SendPackage2> createState() => _SendPackage2State();
}

class _SendPackage2State extends State<SendPackage2> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: colors.background,
        resizeToAvoidBottomInset: false,
        body: Column(
          children: [
            SizedBox(
              height: 108,
              width: double.infinity,
              child: Column(
                children: [
                  SizedBox(
                    height: 106,
                    width: double.infinity,
                    child: Column(
                      children: [
                        const SizedBox(height: 73,),
                        Row(
                          children: [
                            const SizedBox(width: 15),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                              },
                              child: Image.asset('assets/back.png'),
                            ),
                            const SizedBox(width: 99),
                            Text(
                              'Send a package',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontSize: 16),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 2,
                    decoration: const BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(38, 0, 0, 0),
                            blurRadius: 2,
                            offset: Offset(0, 2),
                          )
                        ]
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 24),
                  Text(
                    'Package Information',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: colors.primary
                    ),
                  ),
                  const SizedBox(height: 8,),
                  Text(
                    'Origin details',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: colors.text
                    ),
                  ),
                  const SizedBox(height: 4,),
                  Text(
                      '${widget.pointAddress} , ${widget.pointCountry}',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  const SizedBox(height: 4,),
                  Text(
                      widget.pointPhone,
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  (widget.pointOthers.isNotEmpty) ? Text(
                      widget.pointOthers,
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ) : const SizedBox(),
                  const SizedBox(height: 8,),
                  Text(
                      'Destination details',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.text)
                  ),
                  const SizedBox(height: 4,),
                  Text(
                      '${widget.destinationAddress} , ${widget
                          .destinationCountry}',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  const SizedBox(height: 4,),
                  Text(
                      widget.destinationPhone,
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  (widget.destinationOthers.isNotEmpty) ? Text(
                      widget.destinationOthers,
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ) : const SizedBox(),
                  const SizedBox(height: 8,),
                  Text(
                    'Other details',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: colors.text
                    ),
                  ),
                  const SizedBox(height: 4,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Package Items',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          widget.packageItem,
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 4,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Weight of items',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          widget.packageWeight,
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 4,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Worth of Items',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          widget.packageWorth,
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 4,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Tracking Number',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          'R-7458-4567-4434-5854',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 37,),
                  Divider(height: 1, color: colors.subtext),
                  const SizedBox(height: 8,),
                  Text(
                    'Charges',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: colors.primary
                    ),
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Delivery Charges',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          'N2,500.00',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 8,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Instant delivery',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          'N300.00',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 8,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Tax and Service Charges',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          'N140.00',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 9,),
                  Divider(height: 1, color: colors.subtext),
                  const SizedBox(height: 4,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                          'Package total',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                      ),
                      Text(
                          'N2940.00',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: colors.secondary)
                      ),
                    ],
                  ),
                  const SizedBox(height: 46,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        height: 48,
                        width: 158,
                        child: OutlinedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            style: OutlinedButton.styleFrom(
                                side: BorderSide(color: colors.primary),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)
                                )
                            ),
                            child: Text(
                              'Edit package',
                              style: TextStyle(
                                  color: colors.primary,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700
                              ),
                            )
                        ),
                      ),
                      SizedBox(
                        height: 48,
                        width: 158,
                        child: FilledButton(
                            onPressed: () {
                              pressSendPackage(
                                  widget.pointAddress,
                                  widget.pointCountry,
                                  widget.pointPhone,
                                  widget.pointOthers,
                                  widget.packageItem,
                                  widget.packageWeight,
                                  widget.packageWorth,
                                  2500,
                                  300,
                                  widget.destinationAddress,
                                  widget.destinationCountry,
                                  widget.destinationPhone,
                                  widget.destinationOthers,
                                  (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Transaction()));
                                  },
                                  (String e){showError(context, e);}
                              );
                            },
                            style: FilledButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4)
                              ),
                              backgroundColor: colors.primary,
                            ),
                            child: Text(
                              'Make payment',
                              style: TextStyle(
                                  color: colors.background,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700
                              ),
                            )
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        )
    );
  }
}