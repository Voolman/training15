import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_3/data/models/ModelProfile.dart';
import 'package:training_final_3/domain/ChatRiderPresenter.dart';
import 'package:training_final_3/presentation/widgets/dialogs.dart';

import '../../data/models/ModelMessage.dart';
import '../theme/colors.dart';

class ChatRider extends StatefulWidget {
  final String currentUserId;
  final ModelProfile secondUserId;
  final String chatId;
  const ChatRider({super.key, required this.currentUserId, required this.secondUserId, required this.chatId});


  @override
  State<ChatRider> createState() => _ChatRiderState();
}
List<ModelMessage> messages = [];
class _ChatRiderState extends State<ChatRider> {
  @override
  void initState(){
    super.initState();
    getMessages(
        widget.chatId,
        (res){
          setState(() {
            messages = res;
          });
        },
        (String e){showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 108,
            width: double.infinity,
            decoration: BoxDecoration(
                color: colors.background,
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromARGB(38, 0, 0, 0),
                    blurRadius: 5,
                    offset: Offset(0, 2),
                  )
                ]
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 106,
                  width: double.infinity,
                  child: Column(
                    children: [
                      const SizedBox(height: 73,),
                      Row(
                        children: [
                          const SizedBox(width: 15),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Image.asset('assets/back.png'),
                          ),
                          const SizedBox(width: 66),
                          Container(
                            height: 43,
                            width: 43,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(360),
                              border: Border.all(color: colors.hint, width: 1)
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(360),
                              child: Image.memory(widget.secondUserId.avatar),
                            ),
                          ),
                          Text(
                            widget.secondUserId.fullName,
                            style: Theme
                                .of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(fontSize: 16),
                          ),
                          SizedBox(width: 84),
                          SvgPicture.asset('assets/phone.svg')
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          ListView.builder(
            itemCount: messages.length,
              itemBuilder: (_, index){
                return (messages[index].idUser == widget.currentUserId) ?
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: colors.primary,
                        borderRadius: BorderRadius.circular(8)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Expanded(
                          child: Text(
                            messages[index].message,
                            style: TextStyle(
                              color: colors.background,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              height: 4/3
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ) :
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: colors.hint,
                        borderRadius: BorderRadius.circular(8)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Expanded(
                          child: Text(
                            messages[index].message,
                            style: TextStyle(
                                color: colors.text,
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                height: 4/3
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }
          ),
          Row(
            children: [
              SizedBox(width: 24),
              SvgPicture.asset('assets/emoji.svg'),
              SizedBox(width: 7),
              Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: colors.hint,
                    borderRadius: BorderRadius.circular(8)
                ),
                child: TextField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      hintText: 'Search for a driver',
                      hintStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: colors.subtext
                      ),
                      suffixIcon: SvgPicture.asset(
                          'assets/microphone.svg',
                          fit: BoxFit.scaleDown,
                          colorFilter: ColorFilter.mode(colors.subtext, BlendMode.dst))
                  ),
                ),
              ),
              SvgPicture.asset('assets/triangle.svg')
            ],
          )
        ],
      ),
    );
  }
}