import 'package:flutter/material.dart';
import 'package:training_final_3/presentation/pages/Home.dart';
import 'package:training_final_3/presentation/theme/colors.dart';
import 'package:training_final_3/presentation/widgets/CustomTextField.dart';

import '../../domain/SignUpPresenter.dart';
import '../widgets/dialogs.dart';
import 'LogIn.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});


  @override
  State<SignUp> createState() => _SignUpState();
}
bool isChecked = false;
class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    TextEditingController name = TextEditingController();
    TextEditingController phone = TextEditingController();
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();
    TextEditingController confirmPassword = TextEditingController();
    void onChanged(_){}
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 78, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Create an account',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Complete the sign up process to get started',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 9),
              CustomTextField(
                  label: 'Full name',
                  hint: 'Ivanov Ivan',
                  onChanged: onChanged,
                  controller: name
              ),
              CustomTextField(
                  label: 'Phone Number',
                  hint: '+7(999)999-99-99',
                  onChanged: onChanged,
                  controller: phone
              ),
              CustomTextField(
                  label: 'Email Address',
                  hint: '***********@mail.com',
                  onChanged: onChanged,
                  controller: email
              ),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: password
              ),CustomTextField(
                  label: 'Confirm Password',
                  hint: '**********',
                  enableObscure: true,
                  onChanged: onChanged,
                  controller: confirmPassword
              ),
              const SizedBox(height: 31),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 21,
                    width: 21,
                    child: Checkbox(
                        value: isChecked,
                        activeColor: colors.primary,
                        side: BorderSide(color: colors.subtext, width: 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2)
                        ),
                        onChanged: (value){
                          setState(() {
                            isChecked = value!;
                          });
                        }
                    ),
                  ),
                  const SizedBox(width: 11),
                  Expanded(
                      child: RichText(
                        textAlign: TextAlign.center,
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'By ticking this box, you agree to our ',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12)
                              ),
                              TextSpan(
                                text: 'Terms and conditions and private policy',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12, color: colors.warning)
                              )
                            ]
                          )
                      )
                  ),
                ],
              ),
              const SizedBox(height: 64),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        pressSignUp(
                            email.text,
                            phone.text,
                            name.text,
                            password.text,
                                (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const Home()), (route) => false);},
                                (String e){showError(context, e);}
                        );
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Sign Up',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
              const SizedBox(height: 20),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
                },
                child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Already have an account?',
                          style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                        ),
                        TextSpan(
                          text: 'Sign in',
                          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.primary)
                        )
                      ]
                    )
                ),
              ),
              const SizedBox(height: 18),
              Text(
                'or sign in using',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
              ),
              const SizedBox(height: 8),
              Image.asset('assets/google.png'),
              SizedBox(height: 28,)
            ],
          ),
        ),
      ),
    );
  }
}