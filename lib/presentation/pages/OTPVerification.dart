import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:training_final_3/data/storage/seconds.dart';
import 'package:training_final_3/presentation/theme/colors.dart';
import '../../domain/OTPVerificationPreseneter.dart';
import '../widgets/dialogs.dart';
import 'NewPassword.dart';

class OTPVerification extends StatefulWidget {
  final String email;
  const OTPVerification({super.key, required this.email});


  @override
  State<OTPVerification> createState() => _OTPVerificationState();
}
bool isChecked = false;
class _OTPVerificationState extends State<OTPVerification> {
  @override
  void initState(){
    super.initState();
    timer(
            (){
          setState(() {
          });
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    TextEditingController code = TextEditingController();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 158, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'OTP Verification',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Enter the 6 digit numbers sent to your email',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 52),
              Pinput(
                length: 6,
                controller: code,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                defaultPinTheme: PinTheme(
                  height: 32,
                  width: 32,
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: colors.subtext)
                  ),
                  textStyle: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.black
                  )
                ),
                focusedPinTheme: PinTheme(
                  height: 32,
                  width: 32,
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: colors.primary)
                  ),
                  textStyle: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.black
                  ),
                )
              ),
              const SizedBox(height: 20),
              (lostSeconds != 0) ? Text(
                'If you didn’t receive code, resend after $lostSeconds',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
              ) : GestureDetector(
                onTap: (){
                  setState(() {
                    lostSeconds = 60;
                  });
                },
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'If you didn’t receive code, ',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                      ),
                      TextSpan(
                        text: 'resend',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: colors.primary)
                      )
                    ]
                  ),
                ),
              ),
              const SizedBox(height: 84),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        pressVerifyOTP(
                            widget.email,
                            code.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const NewPassword()));},
                                (String e){showError(context, e);}

                        );
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Set New Password',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}